# Wonderland
[![Build Status](https://travis-ci.org/antarcticatec/wonderland.svg?branch=master)](https://travis-ci.org/antarcticatec/wonderland)

## Down the Rabbit-Hole

	Down, down, down. Would the fall NEVER come to an end! 
	"I wonder how many miles I've fallen by this time?" she said aloud. 
	"I must be getting somewhere near the centre of the earth. 
	 Let me see: that would be four thousand miles down, I think--" 

## The Pool of Tears

	"Curiouser and curiouser!"

	"Let me see: four times five is twelve, and 
	 four times six is thirteen, and four times seven is—oh dear! 
	 I shall never get to twenty at that rate!"

## Alice's Evidence

	"Where shall I begin, please your Majesty?" he asked.
	"Begin at the beginning," the King said gravely, 
	"and go on till you come to the end: then stop."
